SIZE_HEX = 40
SIZE_CURSOR = 6
DIST_CURSOR = 5
SQRT_3_2 = math.sqrt(3) / 2
WIDTH = 840
HEIGHT = 480
HEX_SPEED = 1
CURSOR_SPEED = 4

function love.conf(t)
   t.title = "Super Polygon"
   t.author = "ordiclic"
   t.version = "0.8.0"
   t.screen.width = WIDTH
   t.screen.height = HEIGHT
   t.screen.fullscreen = false
   t.screen.vsync = true
   t.screen.fsaa = 0
   t.modules.joystick = false
   t.modules.audio = false
   t.modules.keyboard = true
   t.modules.event = true
   t.modules.image = false
   t.modules.graphics = true
   t.modules.timer = true
   t.modules.mouse = false
   t.modules.sound = false
   t.modules.physics = false
end
