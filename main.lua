base_triangle = {
    0,             -(SIZE_HEX + DIST_CURSOR) - SIZE_CURSOR,
    SIZE_CURSOR/2, -(SIZE_HEX + DIST_CURSOR),
   -SIZE_CURSOR/2, -(SIZE_HEX + DIST_CURSOR)
}

triangle = {
    0,             -(SIZE_HEX + DIST_CURSOR) - SIZE_CURSOR,
    SIZE_CURSOR/2, -(SIZE_HEX + DIST_CURSOR),
   -SIZE_CURSOR/2, -(SIZE_HEX + DIST_CURSOR)
}


triangle.angle = 0
triangle.direction = 0
triangle.is_rotating = 0

function triangle:rotate(dt)
   if self.is_rotating > 0 then
      self.angle = (self.angle + (dt * self.direction) * math.pi/2 * CURSOR_SPEED) % (2*math.pi)
      for j = 1, 5, 2 do
         triangle[j]   =  (base_triangle[j]) * math.cos(self.angle) + (base_triangle[j+1]) * math.sin(self.angle)
         triangle[j+1] = -(base_triangle[j]) * math.sin(self.angle) + (base_triangle[j+1]) * math.cos(self.angle)
      end
   end
end

hexagon = {
    SIZE_HEX,    0,
    SIZE_HEX/2,  SIZE_HEX*SQRT_3_2,
   -SIZE_HEX/2,  SIZE_HEX*SQRT_3_2,
   -SIZE_HEX,    0,
   -SIZE_HEX/2, -SIZE_HEX*SQRT_3_2,
    SIZE_HEX/2, -SIZE_HEX*SQRT_3_2
}

hexagon.angle = 0

poly_back = {}
for i = 1, 6 do
   poly_back[i] = {}
   poly = poly_back[i]

   poly[1] = hexagon[2*i-1]
   poly[2] = hexagon[2*i]

   poly[3] = hexagon[(2*i%12)+1]
   poly[4] = hexagon[((2*i+1)%12)+1]

   poly[5] = hexagon[(2*i%12)+1]     * 50
   poly[6] = hexagon[((2*i+1)%12)+1] * 50

   poly[7] = hexagon[2*i-1] * 50
   poly[8] = hexagon[2*i]   * 50

   poly.color = {96+32*(i%2),96+32*(i%2),96+32*(i%2)}
end



function love.keypressed(key, unicode)
   if key == 'left' then
      triangle.direction =  1
      triangle.is_rotating = triangle.is_rotating + 1
   elseif key == 'right' then
      triangle.direction = -1
      triangle.is_rotating = triangle.is_rotating + 1
   end
end

function love.keyreleased(key, unicode)
   if key == 'left' then
      triangle.is_rotating = triangle.is_rotating - 1
   elseif key == 'right' then
      triangle.is_rotating = triangle.is_rotating - 1
   end
end

function love.draw()
   love.graphics.translate(WIDTH/2, HEIGHT/2)
   love.graphics.rotate(hexagon.angle)

   -- Every drawing is performed under this line
   for i, poly in ipairs(poly_back) do
      love.graphics.setColor(poly.color)
      love.graphics.polygon('fill', poly)
   end
   love.graphics.setColor(192, 192, 192)
   love.graphics.polygon('line', hexagon)
   love.graphics.setColor(255, 255, 255)
   love.graphics.polygon('line', triangle)
end

function love.update(dt)
   hexagon.angle = (hexagon.angle + dt * math.pi/2 * HEX_SPEED) % (2*math.pi)
   triangle:rotate(dt)
end
